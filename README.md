San QR [Beat81](https://expo.io/@ialberdic/beat81)

# React Native Challenge

Also, version ejected [Beat81](https://gitlab.com/elrolete/beat81-ejected)

## Demo

1. Install [Expo CLI](https://docs.expo.io/versions/latest/workflow/expo-cli/)

```
[sudo] npm install -g expo-cli
```

*If permissions errors then please use `--unsafe-perm=true` flag too [npm/npm#16766](https://github.com/npm/npm/issues/16766)*

###  To do:

Server with socket (WebSockets or Sockets.io) and implement in RN side, in order to allow users join to specific workout

